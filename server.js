const express = require('express');
const app = express();
const path = require('path');
const cors = require("cors");

let corsSettings = {
  origin: true,
  methods: ['POST'],
  credentials: true
};

app.use(express.static(__dirname + '/dist/portfolio'));

app.listen(process.env.PORT || 8080);

app.use(cors(corsSettings));

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT ,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
});

app.get('/*', function(req,res) {
    res.sendFile(path.join(__dirname + '/dist/portfolio/index.html'));
});

console.log('server listening');